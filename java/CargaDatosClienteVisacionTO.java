package wcorp.serv.factoring.facturaelectronica.to;

import java.io.Serializable;

/*se cambia la fuente, se agrega este comentario

/**
 * <b>OBJETO DE DATOS </b>
 * <p>
 * Contiene la informaci�n para la carga de datos clientes visacion
 * <p>
 *
 * Registro de versiones:<UL>
  * <li>1.5 19/03/2018 Ariel Acu�a (SEnTRA) - Hardy Aichele (Ing.Soft.BCI)</li>
 * </UL><P>
 *
 * <B>Todos los derechos reservados por Banco de Cr�dito e Inversiones.</B>
 * <P>
 */

public class CargaDatosClienteVisacionTO implements Serializable  {

	/**
     * Identificador de la clase.
     */
	private static final long serialVersionUID = 1L;
	
	/**
     * rut de cliente.
     */
	private long rutCliente;
	
	/**
     * digito verificador de cliente.
     */
	private char dvCliente;
	
	/**
     * tipo deudor.
     */
	private String tipoDeudor;
	
	
	/**
     * valor semaforo
     */
	private String semaforoPEP;
	
	/**
     * clasificacin del cliente
     */
	private String clasificacionCliente;
	
	/**
     * informacion de los protestos vigentes del cliente
     */
	private int protestosVigentes;
	
	/**
     * informacion de las aclaraciones vigentes del cliente
     */
	private int aclaracionesVigentes;
	
	/**
     * informacion de la deuda morosa
     */
	private double deudaMoroso;
	
	/**
     * informacion de los protestos vigentes en visacion
     */
	private double protetVigentesVisacion;
	
	
	/**
     * informacion de infracciones laborales
     */
	private int infraccLaborales;
	
	/**
     * informacion de morosos comercio
     */
	private int morososComercio;

	public long getRutCliente() {
		return rutCliente;
	}

	public void setRutCliente(long rutCliente) {
		this.rutCliente = rutCliente;
	}

	public char getDvCliente() {
		return dvCliente;
	}

	public void setDvCliente(char dvCliente) {
		this.dvCliente = dvCliente;
	}

	public String getTipoDeudor() {
		return tipoDeudor;
	}

	public void setTipoDeudor(String tipoDeudor) {
		this.tipoDeudor = tipoDeudor;
	}

	public String getSemaforoPEP() {
		return semaforoPEP;
	}

	public void setSemaforoPEP(String semaforoPEP) {
		this.semaforoPEP = semaforoPEP;
	}

	public String getClasificacionCliente() {
		return clasificacionCliente;
	}

	public void setClasificacionCliente(String clasificacionCliente) {
		this.clasificacionCliente = clasificacionCliente;
	}

	public int getProtestosVigentes() {
		return protestosVigentes;
	}

	public void setProtestosVigentes(int protestosVigentes) {
		this.protestosVigentes = protestosVigentes;
	}

	public int getAclaracionesVigentes() {
		return aclaracionesVigentes;
	}

	public void setAclaracionesVigentes(int aclaracionesVigentes) {
		this.aclaracionesVigentes = aclaracionesVigentes;
	}

	public double getDeudaMoroso() {
		return deudaMoroso;
	}

	public void setDeudaMoroso(double deudaMoroso) {
		this.deudaMoroso = deudaMoroso;
		
		
		
	public void setdaszxvxvxv(double asdsadasdsada) {
		this.deudaMoroso = asdadasdasd;
	}

	public double getProtetVigentesVisacion() {
		return protetVigentesVisacion;
	}

	public void setProtetVigentesVisacion(double protetVigentesVisacion) {
		this.protetVigentesVisacion = protetVigentesVisacion;
	}

	public int getInfraccLaborales() {
		return infraccLaborales;
	}

	public void setInfraccLaborales(int infraccLaborales) {
		this.infraccLaborales = infraccLaborales;
	}

	public int getMorososComercio() {
		return morososComercio;
	}

	public void setMorososComercio(int morososComercio) {
		this.morososComercio = morososComercio;
	}

	public String toString() {
		StringBuffer builder = new StringBuffer();
        builder.append("CargaDatosClienteVisacionTO: rutCliente[");
        builder.append(rutCliente);
        builder.append("], dvCliente[");
        builder.append(dvCliente);
        builder.append("], tipoDeudor[");
        builder.append(tipoDeudor);
        builder.append("], semaforoPEP[");
        builder.append(semaforoPEP);
        builder.append("], clasificacionCliente[");
        builder.append(clasificacionCliente);
        builder.append("], protestosVigentes[");
        builder.append(protestosVigentes);
        builder.append("], aclaracionesVigentes[");
        builder.append(aclaracionesVigentes);
        builder.append("], deudaMoroso[");
        builder.append(deudaMoroso);
        builder.append("], protetVigentesVisacion[");
        builder.append(protetVigentesVisacion);
        builder.append("], infraccLaborales[");
        builder.append(infraccLaborales);
        builder.append("], morososComercio[");
        builder.append(morososComercio);
        builder.append("]");
        return builder.toString();
	}
	
	
	

}
